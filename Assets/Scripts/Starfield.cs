﻿using UnityEngine;
using System.Collections;

public class Starfield : MonoBehaviour {

    public float m_sampleRate;
    public float m_density;
    public Camera m_camera;

    private Transform m_transform;

	// Use this for initialization
	void Start () {
        m_transform = transform;

        Object prefab = Resources.Load("Starfield Star");
        GameObject star;
        Transform starTrans;
        Vector3 viewport;
        int iLayer = gameObject.layer;

        for (float i = -0.5f; i < 1.5; i += m_sampleRate) {
            for (float j = -0.5f; j < 1.5; j += m_sampleRate) {
                if (Random.value <= m_density) {
                    star = Instantiate(prefab) as GameObject;
                    star.layer = iLayer;

                    viewport = new Vector3(i + Random.value * 0.1f - 0.05f, j + Random.value * 0.1f - 0.05f, 50);
                    viewport = m_camera.ViewportToWorldPoint(viewport);
                    
                    starTrans = star.transform;
                    starTrans.parent = m_transform;
                    starTrans.position = viewport;
                }
            }
        }
	}
}
