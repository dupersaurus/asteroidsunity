﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Hull))]
public class SpaceObject : Ticker {

    protected Transform m_transform;
    protected Collider2D m_collider;
    protected Rigidbody2D m_rigidbody;
	protected Hull m_hull;

    protected Vector3 m_velocity;

    public float m_fWeight = 1000;
    public float m_fMoI = 1000;

    private static Game m_game;
    private LinkedListNode<Ticker> m_listNode;

    /// <summary>If object has a controller</summary>
    private ShipController m_controller;

    /// <summary>Amount of force to apply on the next tick</summary>
    private Vector3 m_cachedForce;

    /// <summary>Amount of rotation to apply on the next tick</summary>
    private float m_cachedRoation;

    protected virtual void Awake() {
        m_transform = transform;
        m_collider = collider2D;
        m_velocity = Vector3.zero;
        m_rigidbody = rigidbody2D;
        m_controller = GetComponent<ShipController>();
		m_hull = GetComponent<Hull>();
    }

	// Use this for initialization
	protected virtual void Start () {
        m_cachedForce = Vector3.zero;
        m_cachedRoation = 0;
	}

    public Vector3 GetPosition() {
        return m_transform.position;
    }

    public Quaternion GetRotation() {
        return m_transform.rotation;
    }

    public Vector3 GetVelocity() {
        return m_rigidbody.velocity; //m_velocity;
    }

    /// <summary>
    /// Called by projectiles on collision with the space object
    /// </summary>
    /// <param name="damage">Information about the damage being applied</param>
    /// <param name="hit">Parameters of the collision</param>
    /// <returns>The fraction of damage absorbed by the SpaceObject, roughly equal to damageTaken / projectileDamage</returns>
    public virtual float ProjectileCollision(Vector3 velocity, DamageInstance damage, RaycastHit2D hit) {
		return m_hull.ProcessDamage(velocity, damage, hit);
    }

    private void OnCollisionEnter(Collision collision) {
        Debug.Log(this + " >> OnCollisionEnter");
    }

#region ITicker Interface
    public override void Tick(float fDelta) {
        if (m_controller != null) {
            m_controller.Tick(fDelta);
        }

        if (m_cachedRoation != 0) {
            m_transform.rotation *= Quaternion.AngleAxis((m_cachedRoation / m_fMoI) * fDelta, Vector3.forward);
            m_cachedRoation = 0;
            m_rigidbody.angularVelocity = 0;
        }

        // Apply caches
        /*m_velocity += (m_cachedForce / m_fWeight) * fDelta;
        m_transform.rotation *= Quaternion.AngleAxis((m_cachedRoation / m_fMoI) * fDelta, Vector3.forward);

        m_cachedForce = Vector3.zero;
        m_cachedRoation = 0;

        m_transform.position += m_velocity * Time.deltaTime;*/

        base.Tick(fDelta);
    }

    public void SaveNode(Game game, LinkedListNode<Ticker> node) {
        m_game = game;
        m_listNode = node;
    }
#endregion

#region Controller Interaction
    public void ApplyRotation(float fAmt) {
        m_cachedRoation += fAmt;
    }

    public void ApplyForce(Vector3 force) {
        m_rigidbody.AddForce(force);
        //m_cachedForce += force;
    }
#endregion
}
