﻿using UnityEngine;
using System.Collections.Generic;

public class Ticker : MonoBehaviour {
    private Ticker m_nextTicker;
    private Ticker m_previousTicker;

    public Ticker Next {
        get { return m_nextTicker; }
    }

    public Ticker Previous {
        get { return m_previousTicker; }
    }

    /// <summary>
    /// Move to the next ticker
    /// </summary>
    /// <param name="fDelta">Time elapsed since last update</param>
    public virtual void Tick(float fDelta) {
        /*if (m_nextTicker != null) {
            m_nextTicker.Tick(fDelta);
        }*/
    }

    public void InsertAfter(Ticker ticker) {
        m_nextTicker = ticker.m_nextTicker;
        m_previousTicker = ticker;

        ticker.m_nextTicker = this;

        if (m_nextTicker != null) {
            m_nextTicker.m_previousTicker = this;
        }
    }

    /// <summary>
    /// Appends this ticker and its links to a given ticker list.
    /// </summary>
    /// <param name="ticker"></param>
    /// <returns>The last ticker in the resulting list</returns>
    public Ticker AppendToList(Ticker ticker) {
        if (ticker == null) {
            throw new System.NullReferenceException("Ticker::AppendToList must be given a ticker.");
        }

        Ticker insertAfter = ticker;

        while (insertAfter.Next != null) {
            insertAfter = insertAfter.Next;
        }

        insertAfter.m_nextTicker = this;
        this.m_previousTicker = insertAfter;

        // Find new end
        while (insertAfter.Next != null) {
            insertAfter = insertAfter.Next;
        }

        return insertAfter;
    }

    public void RemoveFromTicks() {
        if (m_previousTicker != null) {
            m_previousTicker.m_nextTicker = m_nextTicker;
        }

        if (m_nextTicker != null) {
            m_nextTicker.m_previousTicker = m_previousTicker;
        }

        m_previousTicker = null;
        m_nextTicker = null;
    }
}
