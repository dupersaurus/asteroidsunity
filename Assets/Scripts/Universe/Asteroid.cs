﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Asteroid : SpaceObject {

    public AnimationCurve m_sizeDistribution;
    public AnimationCurve m_healthDistribution;

    public float m_maxSize;
    public float m_maxHealth;

    private float m_fHealth;

    private PoolObject m_pool;
    private float m_fRotationRate;

    protected override void Awake() {
        base.Awake();
        m_pool = GetComponent<PoolObject>();
    }

    protected override void Start() {
        base.Start();
    }

    public override void Tick(float fDelta) {
        //ApplyRotation(m_fRotationRate);
        base.Tick(fDelta);
    }

    public void InitRandom(Vector3 position) {
        ApplyForce(Random.insideUnitCircle * Random.Range(5000, 20000));

        // Randomize size
        InitSpecific(position, m_maxSize * m_sizeDistribution.Evaluate(Random.value));
    }

    public void InitSpecific(Vector3 position, float fScale) {
        m_transform.position = position;
        m_transform.localScale = new Vector3(fScale, fScale, 1);

        m_fHealth = m_maxHealth * m_healthDistribution.Evaluate(fScale / m_maxSize);
    }

    public override float ProjectileCollision(Vector3 velocity, DamageInstance damage, RaycastHit2D hit) {
        DamageType type;
        float fAmount = 0;

		foreach (KeyValuePair<DamageType, DamageDetails> kvp in damage.Details) {
			fAmount += kvp.Value.GetDamage(this);
			type = kvp.Value.GetDamageType();
		}

        m_fHealth -= fAmount;

        if (m_fHealth <= 0) {
            Fracture(damage, hit.point);
        }

        return 1f;
    }

    /// <summary>
    /// Break apart
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="position"></param>
    private void Fracture(DamageInstance damage, Vector3 position) {
        float fScale = m_transform.localScale.x;

        // If too small, just die
        if (fScale <= 2) {
            Game.Instance.RemoveTicker(this);
            m_pool.ReturnToPool();
            return;
        }

        // Split up
        int iPieces;
        int iMinPieces;

        if (fScale <= 4) {
            iPieces = 2;
            iMinPieces = 2;
        } else if (fScale <= 8) {
            iPieces = 4;
            iMinPieces = 2;
        } else {
            iPieces = 6;
            iMinPieces = 4;
        }

        iPieces = Random.Range(iMinPieces, iPieces);
        float fAngle = Random.Range(0f, 359f);
        Vector3 pos;
        Asteroid child;
        float fScaleFactor = Mathf.Sqrt((fScale * fScale * 0.8f) / iPieces);

        for (int i = 0; i < iPieces - 1; i++) {
            pos = m_transform.position + Quaternion.AngleAxis(fAngle, Vector3.forward) * (Vector3.right * fScale * 0.1f);
            child = ObjectPool.SpawnObject<Asteroid>("Asteroid Medium");
            child.InitSpecific(pos, fScaleFactor);
            child.m_velocity = m_velocity;
            child.m_fRotationRate = m_fRotationRate;

            fAngle += 360 / iPieces;
        }

        pos = m_transform.position + Quaternion.AngleAxis(fAngle, Vector3.forward) * (Vector3.right * fScale * 0.1f);
        child = this;
        child.InitSpecific(pos, fScaleFactor);
        child.m_velocity = m_velocity;
    }

    /// <summary>
    /// Called when object has been activated from the pool
    /// </summary>
    protected void PoolActivated() {
        ApplyForce(Random.insideUnitCircle * Random.Range(1000, 10000));
        m_fRotationRate = Random.Range(-5000, 5000);

        Game.Instance.AddTicker(this);
    }
}
