﻿using UnityEngine;
using System.Collections;

public class TargetCaret : MonoBehaviour {

    private Transform m_transform;
    private static Vector3 m_targetPos;

	// Use this for initialization
	void Start () {
        m_transform = transform;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 delta = Vector3.zero;
        delta.x = Input.GetAxis("TargetH");
        delta.y = Input.GetAxis("TargetV");

        m_transform.position += delta * Time.deltaTime * 5;
        m_targetPos = m_transform.position;
	}

    public Vector3 TargetPos {
        get { return m_targetPos; }
    }

    /// <summary>
    /// Attempt to get a target lock on what the cursor is over
    /// </summary>
    public void LockTarget() {

    }
}
