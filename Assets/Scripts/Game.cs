﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {
    private static Game m_instance;

    public static Game Instance {
        get { return m_instance; }
    }

    public PlayerCamera m_playerCam;

    /// <summary>
    /// Start of list of items that want to get tick events
    /// </summary>
    private Ticker m_tickers;
    private Ticker m_lastTicker;

    /// <summary>
    /// Start of list of items that want to get fixed tick events
    /// </summary>
    private Ticker m_fixedTickers;
    private Ticker m_lastFixedTicker;

    /// <summary>Items to add on the next tick</summary>
    private Ticker m_pendingTickers;

    private Ticker m_pendingFixedTickers;

    /// <summary>tickers to remove</summary>
    private List<Ticker> m_deadTickers;

	// Use this for initialization
	void Start () {
        m_instance = this;
        m_deadTickers = new List<Ticker>();

        //Object prefab = Resources.Load("Choo Choo Ship");
        Object prefab = Resources.Load("Gunship");
        //Object prefab = Resources.Load("Boom Boom Ship");
        //Object prefab = Resources.Load("Pew Pew Ship");
        //Object prefab = Resources.Load("LAZOR Ship");

        GameObject ship = Instantiate(prefab) as GameObject;
        AddTicker(ship.GetComponent<SpaceObject>());

        if (m_playerCam != null) {
            m_playerCam.m_target = ship.GetComponent<SpaceObject>();
        }

        // Setup some asteroids
        /*Asteroid asteroid;

        for (int i = 0; i < 25; i++) {
            asteroid = ObjectPool.SpawnObject<Asteroid>("Asteroid Medium");
            asteroid.InitRandom(new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), 0));
        }*/
	}

    void Update() {
		Ticker ticker;

        // Remove dead tickers
        if (m_deadTickers.Count > 0) {
            //Debug.Log("Update >> Remove dead tickers: " + m_deadTickers.Count);
            for (int i = 0; i < m_deadTickers.Count; i++) {
				ticker = m_deadTickers[i];

                if (ticker == m_lastTicker) {
                    m_lastTicker = ticker.Previous;
                }

                ticker.RemoveFromTicks();
            }

            m_deadTickers.Clear();
        }

        // Add pending tickers
        if (m_pendingTickers != null) {
            if (m_tickers != null) {
                //Debug.Log("Update >> Add pending tickers >> Append to list (" + m_lastTicker + ")");
                m_lastTicker = m_pendingTickers.AppendToList(m_lastTicker);
            } else {
                //Debug.Log("Update >> Add pending tickers >> Start list");
                m_tickers = m_pendingTickers;
                
                // Find the last
                while (m_pendingTickers.Next != null) {
                    m_pendingTickers = m_pendingTickers.Next;
                }

                m_lastTicker = m_pendingTickers;
            }

            m_pendingTickers = null;
        }

        // tick
        if (m_tickers != null) {
            ticker = m_tickers;
            
            do {
                ticker.Tick(Time.deltaTime);
                ticker = ticker.Next;
            } while (ticker != null);
        }
    }

    void FixedUpdate() {
        /*if (m_pendingFixedTickers.Count > 0) {
            for (int i = 0; i < m_pendingFixedTickers.Count; i++) {
                m_pendingFixedTickers[i].SaveNode(this, m_fixedTickers.AddLast(m_pendingFixedTickers[i]));
            }

            m_pendingFixedTickers.Clear();
        }

        foreach (Ticker ticker in m_fixedTickers) {
            ticker.Tick(Time.fixedDeltaTime);
        }*/
    }

    public void AddTicker(Ticker ticker) {
        //Debug.Log("AddTicker >> " + ticker + " (" + ticker.GetHashCode() + ") (Existing list? " + (m_pendingTickers != null) + ")");

        if (m_pendingTickers == null) {
            m_pendingTickers = ticker;
        } else {
            //ticker.InsertAfter(m_pendingTickers);
            ticker.AppendToList(m_pendingTickers);
        }
    }

    public void AddFixedTicker(Ticker ticker) {

    }

    public void RemoveTicker(Ticker ticker) {
        //Debug.Log("RemoveTicker >> " + ticker + " (" + ticker.GetHashCode() + ")");
        m_deadTickers.Add(ticker);
    }
}
