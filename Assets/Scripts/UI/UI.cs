﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UI : MonoBehaviour {
    private static UI m_instance;

    public static UI Instance {
        get { return m_instance; }
    }

    private RectTransform m_transform;

    private GunUI m_weaponUI;
    private Canvas m_canvas;

    private List<List<GunUI>> m_weaponUIs;

	// Use this for initialization
	void Awake () {
        m_instance = this;
        m_transform = GetComponent<RectTransform>();
        m_canvas = GetComponent<Canvas>();
	}

    public void StartWeaponSetup() {
        m_weaponUIs = new List<List<GunUI>>();
    }

    public void EndWeaponSetup() {
        float fX = 50;

        foreach (List<GunUI> group in m_weaponUIs) {
            for (int i = 0; i < group.Count; i++) {
                group[i].GetComponent<RectTransform>().Translate(fX, 0, 0);

                fX += 100;
            }
        }
    }

    public void StartWeaponGroup() {
        m_weaponUIs.Add(new List<GunUI>());
    }

    public void EndWeaponGroup() {
        
    }

    public GunUI AddWeaponUI(string sPrefab) {
        GameObject resource = Resources.Load("UI/" + sPrefab) as GameObject;
        GameObject spawn = Instantiate(resource) as GameObject;
        
        //spawn.transform.parent = m_transform;
        //spawn.transform.position = resource.transform.position;

        RectTransform rect = spawn.GetComponent<RectTransform>();
        rect.SetParent(m_transform);
        //rect.Translate(512, 0, 0);

        m_weaponUI = spawn.GetComponent<GunUI>();
        m_weaponUIs[m_weaponUIs.Count - 1].Add(m_weaponUI);

        return m_weaponUI;
    }
}
