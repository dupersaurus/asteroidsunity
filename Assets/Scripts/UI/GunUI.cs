﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GunUI : MonoBehaviour {
    public enum ChargeMode {
        Charged,
        Cooldown,
        Charging,
        Firing
    }

    public RectTransform m_scaler;
    private Image m_scalerImage;

    private float m_fFullHeight;

    void Awake() {
        m_fFullHeight = m_scaler.localScale.y;
        m_scalerImage = m_scaler.GetComponent<Image>();
    }

    public void SetPct(float fPct) {
        fPct = Mathf.Clamp01(fPct);
        m_scaler.localScale = new Vector3(1, m_fFullHeight * fPct, 1);
    }

    public void SetChargeMode(ChargeMode charge) {
        switch (charge) {
            case ChargeMode.Charged:
                m_scalerImage.color = new Color(1, 1, 1, 0.5f);
                break;

            case ChargeMode.Charging:
                m_scalerImage.color = new Color(0, 0.5f, 1, 0.5f);
                break;

            case ChargeMode.Cooldown:
                m_scalerImage.color = new Color(1, 0, 0, 0.5f);
                break;

            case ChargeMode.Firing:
                m_scalerImage.color = new Color(1, 0.5f, 0, 0.5f);
                break;
        }
    }
}
