﻿using UnityEngine;
using System.Collections;

public class SpaceRoad : MonoBehaviour {

    public float m_fStart = 2f;
    public float m_fStep = 0.4f;

    private Transform m_transform;
    private float m_fSpeed;

    private Transform[] m_lines;

	// Use this for initialization
	void Awake () {
        m_transform = transform;

        Object prefab = Resources.Load("Road Line");
        GameObject spawn;
        float fX = m_fStart;
        m_lines = new Transform[10];

        for (int i = 0; i < 10; i++) {
            spawn = Instantiate(prefab) as GameObject;
            m_lines[i] = spawn.transform;

            m_lines[i].parent = m_transform;
            m_lines[i].localPosition = new Vector3(fX, 0, 0);

            fX -= m_fStep;
        }
	}
	
	// Update is called once per frame
	void Update () {
        float fDelta = Time.deltaTime * m_fSpeed;// *2;
        float fX;
        float fAlpha;

        for (int i = 0; i < m_lines.Length; i++) {
            fX = m_lines[i].localPosition.x - fDelta;

            if (fX < m_fStart * -1) {
                fX += m_fStart * 2;
            }

            m_lines[i].localPosition = new Vector3(fX, 0, 0);

            fAlpha = (1 - Mathf.Abs(fX) / m_fStart) * 0.2f;
            fAlpha *= Mathf.Clamp01(m_fSpeed / 0.5f);

            m_lines[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, fAlpha);
        }
	}

    public void SetVelocity(Vector3 position, Vector3 velocity) {
        m_fSpeed = velocity.magnitude;
        m_transform.position = position;
        //m_transform.rotation = Quaternion.LookRotation(velocity);
        m_transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(velocity.y, velocity.x) * 180 / Mathf.PI, Vector3.forward);
    }
}
