﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Island {
    public struct Cell : System.IEquatable<Cell> {
        private int m_iRow;
        private int m_iColumn;

        public Cell(int iRow, int iCol) {
            m_iRow = iRow;
            m_iColumn = iCol;
        }

        public bool Equals(Cell other) {
            return m_iRow == other.m_iRow && m_iColumn == other.m_iColumn;
        }

        public int Row {
            get { return m_iRow; }
        }

        public int Column {
            get { return m_iColumn; }
        }
    }

    /// <summary>
    /// Cell locations that are part of this Island
    /// Would have used C# Tuple, but doing this via Unity which doesn't support those
    /// </summary>
    private HashSet<Cell> m_cells;

    /// <summary>
    /// Create an island with an initially-seeded row
    /// </summary>
    /// <param name="iRow"></param>
    /// <param name="iMinCol"></param>
    /// <param name="iMaxCol"></param>
    public Island(int iRow, int iMinCol, int iMaxCol) {
        m_cells = new HashSet<Cell>();

        for (int i = iMinCol; i <= iMaxCol; i++) {
            m_cells.Add(new Cell(iRow, i));
        }
    }

    /// <summary>
    /// Given a range of cells on a row, check to add the cells to the island.
    /// If adjacent, add to the island.
    /// </summary>
    /// <param name="iRow">Row in question</param>
    /// <param name="iMinCol">Start of the column range</param>
    /// <param name="iMaxCol">End of the column range</param>
    /// <returns>True/false</returns>
    public bool DetermineAdjacency(int iRow, int iMinCol, int iMaxCol) {
        bool bAdjacent = false;

        for (int i = iMinCol; i <= iMaxCol; i++) {
            if (m_cells.Contains(new Cell(iRow - 1, i))) {
                bAdjacent = true;
            }    
        }

        // Add chunk to island
        if (bAdjacent) {
            for (int i = iMinCol; i <= iMaxCol; i++) {
                m_cells.Add(new Cell(iRow, i));
            }
        }

        return bAdjacent;
    }

    /// <summary>
    /// If any cells of a given island collide with this one, merge the other island into this one.
    /// </summary>
    /// <param name="island">The island to check</param>
    /// <returns>True if combined, false if not</returns>
    public bool CombineWith(Island other) {
        if (m_cells.Overlaps(other.m_cells)) {
            m_cells.UnionWith(other.m_cells);
            return true;
        }

        return false;
    }
}

public class IslandCounter : MonoBehaviour {

    private string m_sMap = "000000000000000000" +
                            "011100000000000000" +
                            "000110000000000000" +
                            "001110000000000000" +
                            "000000001010001110" +
                            "000010011110001100" +
                            "000011111110011100" +
                            "000011111011100000" +
                            "000000001110000000";

	// Use this for initialization
	void Start () {
        Debug.Log(CountIslands(m_sMap, 18, 9) + " islands found");
	}

    /// <summary>
    /// Go through each row incrementally to find islands. 
    /// </summary>
    /// <param name="sMap"></param>
    /// <param name="iColCount">Number of columns in the map grid</param>
    /// <param name="iRowCount">Number of rows in the map grid</param>
    /// <returns></returns>
    private int CountIslands(string sMap, int iColCount, int iRowCount) {

        List<Island> islands = new List<Island>();

        // Bounds of land on a row. For each pair of values, first is the starting column, second is the end
        int[] landRanges = new int[iColCount];
        int iLandRangeIndex = 0;

        bool bWasLastLand = false;

        for (int iRow = 0; iRow < iRowCount; iRow++) {
            // Initialize
            bWasLastLand = false;

            for (int i = 0; i < iColCount; i++) {
                landRanges[i] = -1;
            }

            iLandRangeIndex = 0;

            // Find groups of land on this row
            for (int iCol = 0; iCol < iColCount; iCol++) {
                if (sMap[iRow * iColCount + iCol] == '1') {
                    if (!bWasLastLand) {
                        landRanges[iLandRangeIndex] = iCol;
                        iLandRangeIndex++;
                    }

                    bWasLastLand = true;
                } else {
                    if (bWasLastLand) {
                        landRanges[iLandRangeIndex] = iCol - 1;
                        iLandRangeIndex++;
                    }

                    bWasLastLand = false;
                }
            }

            string sEcho = "Row " + iRow + " >> ";

            for (int i = 0; i < iColCount; i++) {
                if (landRanges[i] == -1) {
                    break;
                }

                sEcho += landRanges[i] + ", ";
            }

            // Add to islands, or create new ones
            bool bNewIsland;
            List<Island> newIslands = new List<Island>();

            for (int iPair = 0; iPair < iColCount; iPair += 2) {
                if (landRanges[iPair] == -1) {
                    break;
                }

                bNewIsland = true;

                for (int iIsland = 0; iIsland < islands.Count; iIsland++) {
                    if (islands[iIsland] == null) {
                        continue;
                    }

                    if (islands[iIsland].DetermineAdjacency(iRow, landRanges[iPair], landRanges[iPair + 1])) {
                        
                        // Don't need to make a new island, but continue looking for adjacencies
                        bNewIsland = false;
                    }
                }

                // No matches, create a new island. Cache it to add to the main list later so it doesn't become part of the loop above.
                if (bNewIsland) {
                    Debug.Log("New Island >> On " + iRow + ", from " + landRanges[iPair] + " to " + landRanges[iPair + 1]);
                    newIslands.Add(new Island(iRow, landRanges[iPair], landRanges[iPair + 1]));
                } else {
                    Debug.Log("Added to island >> " + iRow + ", from " + landRanges[iPair] + " to " + landRanges[iPair + 1]);
                }
            }

            if (newIslands.Count > 0) {
                islands.AddRange(newIslands);
                newIslands.Clear();
            }
            
            // Combine islands that are now linked together
            for (int i = 0; i < islands.Count - 1; i++) {
                if (islands[i] == null) {
                    continue;
                }
                
                for (int j = i + 1; j < islands.Count; j++) {
                    if (islands[j] == null) {
                        continue;
                    }

                    if (islands[i].CombineWith(islands[j])) {
                        Debug.Log("Island overlap");
                        islands[j] = null;
                    }
                }
            }
        }

        // Count
        int iCount = 0;

        for (int i = 0; i < islands.Count - 1; i++) {
            if (islands[i] != null) {
                iCount++;
            }
        }

        return iCount;
    }
}
