﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Types of damage
/// </summary>
public enum DamageType {
    Penetrating,
    Blast,
    Thermal
}

/// <summary>
/// Information about damage applied in a single hit
/// </summary>
/// <seealso cref="DamageDetails"/>
public class DamageInstance {
    protected Dictionary<DamageType, DamageDetails> m_details;

    /// <summary>
    /// All damage types applied by this damage
    /// </summary>
	public Dictionary<DamageType, DamageDetails> Details {
        get { return m_details; }
    }

    /// <summary>
    /// Create a damage descriptor with certain types of details
    /// </summary>
    /// <param name="details"></param>
    public DamageInstance() {
		m_details = new Dictionary<DamageType, DamageDetails>();
    }

	public PenetratingDamage Penetrating {
		get {
			if (m_details.ContainsKey(DamageType.Penetrating)) {
				return (PenetratingDamage)m_details[DamageType.Penetrating];
			} else {
				return null;
			}
		}

		set {
			if (!(value is PenetratingDamage)) {
				throw new NotSupportedException("DamageDetails must be of type PenetratingDamage");
			}

			m_details[DamageType.Penetrating] = value;
		}
	}

	public BlastDamage Blast {
		get {
			if (m_details.ContainsKey(DamageType.Blast)) {
				return (BlastDamage)m_details[DamageType.Blast];
			} else {
				return null;
			}
		}

		set {
            if (!(value is BlastDamage)) {
                throw new NotSupportedException("DamageDetails must be of type BlastDamage");
            }

			m_details[DamageType.Blast] = value;
		}
	}

	public DamageDetails Thermal {
		get {
			if (m_details.ContainsKey(DamageType.Thermal)) {
				return m_details[DamageType.Thermal];
			} else {
				return null;
			}
		}

		set {
			m_details[DamageType.Thermal] = value;
		}
	}
}

/// <summary>
/// Describes damage applied by a certain DamageType
/// </summary>
/// <seealso cref="DamageType"/>
public class DamageDetails {
    protected DamageType m_type;
    protected float m_fDamage;

    public DamageDetails(DamageType type, float fAmount) {
        m_type = type;
        m_fDamage = fAmount;
    }

    /// <summary>
    /// Gets damage to apply to a collider. 
    /// This gives the damage a chance to modify itself based on what it's hitting.
    /// The thing being hit will handle its own damaage mitigation after this.
    /// </summary>
    /// <param name="collider"></param>
    public float GetDamage(SpaceObject collider) {
        return m_fDamage;
    }

    public DamageType GetDamageType() {
        return m_type;
    }
}

// TODO DamageDetail subtypes for different damage types?
public class PenetratingDamage : DamageDetails {

	/// <summary>
	/// Measure of penetrating power.
	/// TODO Figure out scale, maybe unit?
	/// </summary>
	protected float m_fPenetrationFactor;

	/// <summary>
	/// After a successful penetration, used to calculate penetrating distance by scaling penetration factor by this.
	/// </summary>
	protected float m_fFalloff;

	public PenetratingDamage(float fDamage, float fPenetration, float fFalloff) : base(DamageType.Penetrating, fDamage) {
		m_fPenetrationFactor = fPenetration;
		m_fFalloff = fFalloff;
	}

	public float Penetration {
		get { return m_fPenetrationFactor; }
	}

	public float Falloff {
		get { return m_fFalloff; }
	}
}

public class BlastDamage : DamageDetails {
    
    /// <summary>
    /// Distance to travel before exploding
    /// </summary>
    protected float m_fFuseLength;

    public BlastDamage(float fDamage, float fFuse)
        : base(DamageType.Blast, fDamage) {
            m_fFuseLength = fFuse;
    }

    public float FuseLength {
        get { return m_fFuseLength; }
    }
}