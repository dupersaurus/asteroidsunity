﻿using UnityEngine;
using System.Collections;

public class PlayerController : ShipController {

    private SpaceObject m_controlled;
    private Transform m_transform;

    private float m_fLastThrust = 0;

    private SpaceRoad m_spaceRoad;

	// Use this for initialization
    protected override void Awake() {
        base.Awake();

        m_controlled = GetComponent<SpaceObject>();
        m_transform = transform;

        m_spaceRoad = (Instantiate(Resources.Load("Space Road")) as GameObject).GetComponent<SpaceRoad>();
	}
	
#region ITicker Interface

    /// <summary>
    /// Handle inputs
    /// </summary>
    /// <param name="fDelta"></param>
    public override void Tick(float fDelta) {

        base.Tick(fDelta);

        // Rotate object
        float fRotation = Input.GetAxis("Rotation");
        m_controlled.ApplyRotation(fRotation * m_rcs.GetRotationForce());

        // Calculate thrust
        Vector3 netForce = Vector3.zero;
        netForce.x = Input.GetAxis("Vertical");
        netForce.y = Input.GetAxis("Horizontal");

        if (netForce.x > 0 && m_fLastThrust <= 0) {
            m_engine.StartThrust();
        } else if (netForce.x <= 0 && m_fLastThrust > 0) {
            m_engine.EndThrust();
        }

        m_fLastThrust = netForce.x;

        if (netForce.x > 0) {
            netForce.x *= m_engine.GetThrust();
        } else {
            netForce.x *= m_rcs.GetTranslationForce();
        }

        netForce.y *= m_rcs.GetTranslationForce();

        // Force is applied along the facing of the object
        netForce = m_transform.rotation * netForce;

        m_controlled.ApplyForce(netForce);

        // Weapons
        CheckFiring(0);
        CheckFiring(1);

        m_spaceRoad.SetVelocity(m_transform.position, m_controlled.GetVelocity());
    }
#endregion

    protected void CheckFiring(byte iGroup) {
        string sBinding = "";

        switch (iGroup) {
            case 0: sBinding = "Fire1"; break;
            case 1: sBinding = "Fire2"; break;
            case 2: sBinding = "Fire3"; break;

            default:
                throw new System.ArgumentOutOfRangeException("Weapon group " + iGroup + " invalid.");
        }

        if (Input.GetButtonDown(sBinding)) {
            BeginFireWeaponGroup(iGroup);
        } else if (Input.GetButtonUp(sBinding)) {
            EndFireWeaponGroup(iGroup);
        }
    }
}
