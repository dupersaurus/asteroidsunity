﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(SpaceObject))]
[RequireComponent(typeof(BaseRCS))]
[RequireComponent(typeof(BaseEngine))]
public class ShipController : MonoBehaviour {

    [SerializeField]
    private List<WeaponGroup> m_groups;

    protected BaseEngine m_engine;
    protected BaseRCS m_rcs;
    protected FireControl m_fireControl;

    private LinkedListNode<Ticker> m_linkNode;

	// Use this for initialization
	protected virtual void Awake () {
        m_engine = GetComponent<BaseEngine>();
        m_rcs = GetComponent<BaseRCS>();
        m_fireControl = GetComponent<FireControl>();
	}

    void Start() {
        UI.Instance.StartWeaponSetup();

        for (int i = 0; i < m_groups.Count; i++) {
            m_groups[i].Setup();
        }

        UI.Instance.EndWeaponSetup();
    }

    public virtual void Tick(float fDelta) {
        if (m_engine != null)   m_engine.Tick(fDelta);
        if (m_rcs != null)      m_rcs.Tick(fDelta);
        if (m_fireControl != null) m_fireControl.Tick(fDelta);

        TickWeaponGroups(fDelta);
    }

    protected void TickWeaponGroups(float fDelta) {
        for (byte i = 0; i < m_groups.Count; i++) {
            m_groups[i].Tick(fDelta);
        }
    }

    protected void BeginFireWeaponGroup(byte iIndex) {
        if (iIndex >= m_groups.Count || m_groups[iIndex] == null) {
            return;
        }

        m_groups[iIndex].BeginFiring();
    }

    protected void EndFireWeaponGroup(byte iIndex) {
        if (iIndex >= m_groups.Count || m_groups[iIndex] == null) {
            return;
        }

        m_groups[iIndex].EndFiring();
    }
}

[System.Serializable]
public class WeaponGroup {

    [SerializeField]
    protected List<BaseWeapon> m_weapons;

    public void Setup() {
        UI.Instance.StartWeaponGroup();

        for (byte i = 0; i < m_weapons.Count; i++) {
            m_weapons[i].AddWeaponUI();
        }

        UI.Instance.EndWeaponGroup();
    }

    public void Tick(float fDelta) {
        for (byte i = 0; i < m_weapons.Count; i++) {
            m_weapons[i].Tick(fDelta);
        }
    }

    public void BeginFiring() {
        for (byte i = 0; i < m_weapons.Count; i++) {
            m_weapons[i].BeginFiring();
        }
    }

    public void EndFiring() {
        for (byte i = 0; i < m_weapons.Count; i++) {
            m_weapons[i].EndFiring();
        }
    }
}