﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Simple kinetic projectile.
/// </summary>
[RequireComponent(typeof(PoolObject))]
public class Projectile : Ticker {

    protected Transform m_transform;
    public ParticleSystem m_particle;
    private PoolObject m_pool;

    private static Game m_game;
    private LinkedListNode<Ticker> m_listNode;

    /// <summary>Collider of the object that spawned the projectile</summary>
    private Transform m_parentTransform;

    /// <summary>Velocity</summary>
    protected Vector3 m_velocity;

    // Projectile definitions
    /// <summary>How long projectile lives, in seconds</summary>
    public float m_lifeSpan;

    /// <summary>Base damage. Will be modified based on specific projectile needs.</summary>
    public float m_baseDamage;

    public float m_penetratingFactor = 5;

    /// <summary>Time left before dying</summary>
    private float m_fLifeLeft;

    /// <summary>Damage applied by this projectile</summary>
    protected DamageInstance m_damageInfo;

    /// <summary>Projectile to spawn at the hit point</summary>
    public string m_collisionProjectile = "Bullet Hit";

    private bool m_bCollide = true;

	// Use this for initialization
	protected virtual void Awake () {
        m_transform = transform;
        m_pool = GetComponent<PoolObject>();

        if (m_particle == null) {
            m_particle = GetComponentInChildren<ParticleSystem>();
        }
	}

    #region Creation
    /// <summary>
    /// Initialize the position and movement of the projectile at launch.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="velocity"></param>
    /// <param name="rotation"></param>
    public virtual void SetLaunchParameters(Vector3 position, Vector3 velocity, Quaternion rotation) {
        if (m_transform == null) {
            m_transform = transform;
        }

        m_transform.position = position;
        m_transform.rotation = rotation * Quaternion.AngleAxis(-90, Vector3.forward);

        m_velocity = velocity;
        m_fLifeLeft = m_lifeSpan;

        SetDamageDescription();
    }

    /// <summary>
    /// The object that launched the projectile
    /// </summary>
    public Transform Launcher {
        get { return m_parentTransform; }
        set { m_parentTransform = value; }
    }

    /// <summary>
    /// Set the damage information for the projectile. 
    /// Projectile is a simple kinetic mass whose damage scales by velocity.
    /// </summary>
    protected virtual void SetDamageDescription() {
        m_damageInfo = new DamageInstance();
		m_damageInfo.Penetrating = new PenetratingDamage(m_baseDamage * m_velocity.magnitude * 0.1f,
                                                         m_penetratingFactor * m_velocity.magnitude * 0.1f, 0.5f);
    }
        
    protected virtual void PoolActivated() {
        m_bCollide = true;
        m_particle.Play();
    }

    #endregion

    #region Ticking

    /// <summary>
    /// Called each update
    /// </summary>
    /// <param name="fDelta"></param>
    public override void Tick(float fDelta) {
        // Distance traveled this frame
        Vector3 travel = m_velocity * fDelta;

        // Check for collisions
        RaycastHit2D hit = Physics2D.Raycast(m_transform.position, travel, travel.magnitude);
        bool bAlive = true;

        if (m_bCollide) {
            if (hit.collider != null && hit.transform != m_parentTransform) {
                SpaceObject spaceObj = hit.transform.GetComponent<SpaceObject>();

                if (spaceObj != null) {
                    float fCollisionAbsorb = spaceObj.ProjectileCollision(m_velocity, m_damageInfo, hit);
                    bAlive = RespondToCollision(hit, fCollisionAbsorb);
                }
            }
        }

        // Apply
        if (bAlive) {
            m_transform.position += travel;

            m_fLifeLeft -= fDelta;
        }

        if (!bAlive || m_fLifeLeft <= 0) {
            base.Tick(fDelta);
            Die();
        }
    }

    /// <summary>
    /// Respond to a collision
    /// </summary>
    /// <param name="hit">The collision info</param>
    /// <param name="fAbsorbFactor">Amount of damage that the collider absorbed</param>
    /// <returns>True if the projectile is still alive, false if not</returns>
    protected virtual bool RespondToCollision(RaycastHit2D hit, float fAbsorbFactor) {
        // If enough of the damage is absorbed, projectile dies...
        if (fAbsorbFactor >= 1f) {
            SpawnParticleAt(m_collisionProjectile, hit.point, hit.normal);

            // Deflect
            if (fAbsorbFactor > 1f) {
                float fDot = Vector2.Dot(hit.normal, m_velocity.normalized);

                if (Mathf.Abs(fDot) >= 0.75f) {
                    m_velocity = Vector3.Reflect(m_velocity, hit.normal) * 0.25f;
                    m_transform.position = hit.point;
                    m_bCollide = false;
                    m_fLifeLeft = 0.5f * Random.value;

                    if (m_particle != null) {
                        m_particle.Stop();
                    }

                    return true;
                }
            }

            return false;
        }

        // ...otherwise, slow it down relative to damage left
        else {

        }

        return true;
    }

    protected void SpawnParticleAt(string sParticle, Vector3 pos, Vector3 normal) {
        GameObject particle = ObjectPool.SpawnObject(sParticle);

        if (particle != null) {
            Transform particleTrans = particle.transform;
            particleTrans.parent = m_transform.parent;
            particleTrans.position = pos;
            particleTrans.rotation = Quaternion.LookRotation(normal);
        }
    }

    /// <summary>
    /// Cleanup after dying
    /// </summary>
    protected virtual void Die() {
        Game.Instance.RemoveTicker(this);
        m_pool.ReturnToPool();
    }

    /// <summary>
    /// Save our position in the ticker list for quick removal
    /// </summary>
    /// <param name="game"></param>
    /// <param name="node"></param>
    public void SaveNode(Game game, LinkedListNode<Ticker> node) {
        m_game = game;
        m_listNode = node;
    }
    #endregion

    #region Colliding

    #endregion
}
