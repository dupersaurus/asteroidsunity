﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A self-propelled projectile.
/// </summary>
public class Missile : Projectile {

    /// <summary>Change in velocity available, unit/sec</summary>
    public float m_deltaV;

    /// <summary>Rate of acceleration at full throttle, in terms of dV consumed per second</summary>
    public float m_deltaVRate;

    /// <summary>Time to wait after launch before firing motor</summary>
    public float m_launchDelay;

    /// <summary>
    /// Distance to travel after penetrating before exploding
    /// </summary>
    public float m_fuseLength;

    /// <summary>
    /// How far away from a failed intercept triggers detonation.
    /// </summary>
    public float m_fuseDistance;

    /// <summary>Rate of turn, degrees/sec</summary>
    public float m_turnRate;

    private float m_fThrottle;
    private float m_fFireCountdown;
    private float m_fDVRemaining;

    private bool m_bUnderGuidance = false;
    private SpaceObject m_targetObject;
    private Vector3 m_targetPos;
    private float m_fLastDistance = float.MaxValue;

    public override void SetLaunchParameters(Vector3 position, Vector3 velocity, Quaternion rotation) {
        m_fThrottle = 1;
        m_fFireCountdown = m_launchDelay;
        m_fDVRemaining = m_deltaV;
        m_fLastDistance = float.MaxValue;
        
        base.SetLaunchParameters(position, velocity, rotation);
    }

    protected override void SetDamageDescription() {
        m_damageInfo = new DamageInstance();
		m_damageInfo.Blast = new BlastDamage(m_baseDamage, m_fuseLength);

        if (m_penetratingFactor > 0) {
            m_damageInfo.Penetrating = new PenetratingDamage(0, m_penetratingFactor * m_velocity.magnitude * 0.1f, 1);
        }
    }

    public override void Tick(float fDelta) {
        // Clear launcher
        if (m_fFireCountdown > 0) {
            m_fFireCountdown -= fDelta;

            if (m_fFireCountdown <= 0 && m_particle != null) {
                m_particle.Play();
            }

        } 
        
        // Maneuver
        else if (m_fThrottle > 0 && m_fDVRemaining > 0) {
            bool bPower = true;

            // Steer
            if (m_bUnderGuidance) {
                Vector3 target;

                if (m_targetObject != null) {
                    target = m_targetObject.GetPosition();

                    // TODO calculate lead
                } else {
                    target = m_targetPos;
                }

                Vector3 diff = target - m_transform.position;
                diff.z = 0;
                Vector3 cross = Vector3.Cross(m_transform.rotation * Vector3.up, diff);
                float fAngle = Vector3.Angle(m_transform.rotation * Vector3.up, diff);
                float fDistance = diff.sqrMagnitude;
                float fTurnAngle = 0;

                Debug.Log("Last: " + m_fLastDistance + ", Distance: " + fDistance + ", Fuse: " + (m_fuseDistance * m_fuseDistance));

                if (fDistance > m_fLastDistance && m_fLastDistance <= m_fuseDistance * m_fuseDistance) {
                    Explode();
                    return;
                }

                m_fLastDistance = fDistance;

                if (fAngle > 20) {
                    bPower = false;
                }

                if (m_turnRate * fDelta > fAngle) {
                    fTurnAngle = fAngle;
                } else {
                    fTurnAngle = m_turnRate * fDelta;
                }

                if (cross.z < 0) {
                    fTurnAngle *= -1;
                }

                Quaternion rot = Quaternion.AngleAxis(fTurnAngle, Vector3.forward);
                m_velocity = rot * m_velocity;
                m_transform.rotation = rot * m_transform.rotation;
            }

            // Accelerate
            float fThrust = m_deltaVRate * fDelta;
            Vector3 acceleration = Vector3.zero;

            if (bPower) {
                if (fThrust > m_fDVRemaining) {
                    fThrust = m_fDVRemaining;
                }

                m_fDVRemaining -= fThrust;
                acceleration = m_transform.rotation * (Vector3.up * fThrust);
                m_velocity += acceleration;
            }
        }
        
        base.Tick(fDelta);
    }

    protected override bool RespondToCollision(RaycastHit2D hit, float fAbsorbFactor) {
        SpawnParticleAt(m_collisionProjectile, hit.point, hit.normal);
        return false;
    }

    protected void Explode() {
        Die();
    }

    protected override void Die() {
        if (m_particle != null) {
            m_particle.Stop();
        }
        
        base.Die();
    }

    /// <summary>
    /// Target a space object
    /// </summary>
    /// <param name="target"></param>
    public void SetTarget(SpaceObject target) {
        m_targetObject = target;
        m_bUnderGuidance = true;
    }

    /// <summary>
    /// Set a target position
    /// </summary>
    /// <param name="target"></param>
    public void SetTarget(Vector3 target) {
        m_targetPos = target;
        m_bUnderGuidance = true;
    }

    /// <summary>
    /// Set a target starting a certain position, traveling a certain velocity
    /// </summary>
    /// <param name="target"></param>
    /// <param name="velocity"></param>
    public void SetTarget(Vector3 target, Vector3 velocity) {
        m_targetPos = target;
        m_bUnderGuidance = true;
    }
}
