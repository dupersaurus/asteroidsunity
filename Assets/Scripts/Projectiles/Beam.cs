﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PoolObject))]
public class Beam : MonoBehaviour {
    
    /// <summary>Maximum length of the beam</summary>
    public float m_range;

    /// <summary>Profile of damage amount along its max range</summary>
    public AnimationCurve m_damageProfile;

    private Transform m_transform;
    private PoolObject m_pool;
    private SpriteRenderer m_sprite;

    private int m_iUpdateCount = 0;

    protected DamageInstance m_damageInfo;

    void Awake() {
        m_transform = transform;
        m_pool = GetComponent<PoolObject>();
        m_sprite = GetComponentInChildren<SpriteRenderer>();
    }

    public void UpdateFiringParams(Vector3 pos, Quaternion rotation, float fStrength) {
        m_iUpdateCount++;

        float fDistance = m_range;
        RaycastHit2D ray = Physics2D.Raycast(pos, rotation * Vector3.right, m_range);

        if (ray.collider != null) {
            fDistance = m_range * ray.fraction;

            SpaceObject spaceObj = ray.transform.GetComponent<SpaceObject>();

            if (spaceObj != null) {
				DamageInstance damage = new DamageInstance();
				damage.Thermal = new DamageDetails(DamageType.Thermal, fStrength * m_damageProfile.Evaluate(ray.fraction));

                spaceObj.ProjectileCollision(rotation * Vector3.right, damage, ray);

                if (m_iUpdateCount % 10 == 0) {
                    GameObject particle = ObjectPool.SpawnObject("Bullet Hit");

                    if (particle != null) {
                        Transform particleTrans = particle.transform;
                        particleTrans.parent = m_transform.parent;
                        particleTrans.position = ray.point;
                        particleTrans.rotation = Quaternion.LookRotation(ray.normal);
                    }
                }
            }
        }

        // Draw
        m_transform.position = pos;
        m_transform.rotation = rotation;
        m_transform.localScale = new Vector3(fDistance, m_transform.localScale.y, 1);

        m_sprite.color = new Color(m_sprite.color.r, m_sprite.color.g, m_sprite.color.b, fStrength);
    }

    public void StopFiring() {
        m_pool.ReturnToPool();
    }
}
