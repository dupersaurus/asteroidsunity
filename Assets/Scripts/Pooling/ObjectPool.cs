﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour {

    private static ObjectPool m_instance;

    public static ObjectPool Instance {
        get { return m_instance; }
    }

    private Transform m_transform;

    public static Transform Transform {
        get { return m_instance.m_transform; }
    }

    /// <summary>
    /// Definition for an object in the pool, defined in the editor.
    /// </summary>
    [System.Serializable]
    public struct ObjectDefinition {

        /// <summary>Id used to reference the object</summary>
        public string ObjectName;

        /// <summary>Path to the prefab in resources</summary>
        public string ResourcePath;

        /// <summary>Size of initial buffer</summary>
        public int BufferSize;
    }

    /// <summary>All object defintions, set in the editor</summary>
    public List<ObjectDefinition> m_objectDefinitions;

    /// <summary>
    /// A pool of objects
    /// </summary>
    private class Pool {
        /// <summary>Object info</summary>
        public ObjectDefinition m_definition;

        /// <summary>Time at which an object was last requested</summary>
        public float m_fLastAccessTime;

        /// <summary>All objects in the pool</summary>
        public LinkedList<PoolObject> m_objects;

        private Object m_prefab;

        public Pool(ObjectDefinition definition) {
            m_definition = definition;
            m_fLastAccessTime = 0;
            m_objects = new LinkedList<PoolObject>();
            m_prefab = null;

            SeedPool();
        }

        /// <summary>
        /// Requests an object from the pool. If no free objects are available, spawns a new batch.
        /// </summary>
        /// <returns></returns>
        public GameObject RequestObject() {

            // Find first inactive object
            foreach (PoolObject obj in m_objects) {
                if (!obj.IsActive) {
                    obj.Activate();
                    return obj.gameObject;
                }
            }

            // No free objects, increase pool size
            return SeedPool().gameObject;
        }

        /// <summary>
        /// Fills the pool with objects
        /// </summary>
        /// <returns>The an object created in the seed</returns>
        private PoolObject SeedPool() {
            PoolObject obj = null;

            if (m_prefab == null) {
                m_prefab = Resources.Load(m_definition.ResourcePath);
            }

            for (int i = 0; i < m_definition.BufferSize; i++) {
                obj = (Instantiate(m_prefab) as GameObject).GetComponent<PoolObject>();

                if (obj == null) {
                    Debug.LogError("Object loaded at \"" + m_definition.ResourcePath + "\" does not have the PoolObject script on it");
                    return null;
                }

                m_objects.AddLast(obj);
                obj.Initialize(m_definition.ObjectName);
            }

            return obj;
        }
    }

    /// <summary>
    /// All object pools
    /// </summary>
    private Dictionary<string, Pool> m_pools;

    void Awake() {
        m_instance = this;
        m_transform = transform;
        m_pools = new Dictionary<string, Pool>();

        for (int i = 0; i < m_objectDefinitions.Count; i++) {
            m_pools.Add(m_objectDefinitions[i].ObjectName, new Pool(m_objectDefinitions[i]));
        }
    }

    /// <summary>
    /// Requests an instance of an object from the pool
    /// </summary>
    /// <param name="sType">The id of the object to spawn</param>
    /// <returns>A spawned object, or null if error</returns>
    public static GameObject SpawnObject(string sType) {
        if (Instance != null) {
            return Instance._SpawnObject(sType);
        } else {
            return null;
        }
    }

    public static T SpawnObject<T>(string sType) where T : Component {
        GameObject spawn = SpawnObject(sType);

        if (spawn == null) {
            return null;
        } else {
            return spawn.GetComponent<T>();
        }
    }

    private GameObject _SpawnObject(string sType) {
        return m_pools[sType].RequestObject();
    }
}
