﻿using UnityEngine;
using System.Collections;

public class ParticlePoolObject : PoolObject {
    private ParticleSystem m_particle;

	// Use this for initialization
	void Awake () {
        m_particle = particleSystem;
	}
	
	// Update is called once per frame
	void Update () {
        if (m_particle != null && !m_particle.IsAlive()) {
            ReturnToPool();
        }
	}
}
