﻿using UnityEngine;
using System.Collections;

public class PoolObject : MonoBehaviour {

    private GameObject m_gameObject;

    /// <summary>Whether the object is active in the pool</summary>
    private bool m_bIsActive = false;

    /// <summary>Whether the object is active in the pool</summary>
    public bool IsActive {
        get { return m_bIsActive; }
    }

    /// <summary>The object's pool id</summary>
    private string m_sPoolId;

    private Transform m_transform;

    public virtual void Initialize(string sPoolId) {
        m_sPoolId = sPoolId;
        m_transform = transform;
        m_transform.parent = ObjectPool.Transform;

        m_gameObject = gameObject;
        m_gameObject.SetActive(false);
    }

    /// <summary>
    /// Called when activated from the pool
    /// </summary>
    public virtual void Activate() {
        m_bIsActive = true;
        m_gameObject.SetActive(true);
        SendMessage("PoolActivated", SendMessageOptions.DontRequireReceiver);
    }

    /// <summary>
    /// Returns the object to the pool
    /// </summary>
    public virtual void ReturnToPool() {
        m_bIsActive = false;
        m_transform.parent = ObjectPool.Transform;
        m_gameObject.SetActive(false);
    }
}
