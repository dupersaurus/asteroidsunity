﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A kinetic weapon whose power is proportional to how long it is charged
/// </summary>
public class ChargeGun : BaseWeapon {

    // *********** Inspector Fields ***************************************************************

    /// <summary>Minimum time to charge to fire</summary>
    [SerializeField]
    private float m_minChargeTime;

    /// <summary>Power at minimum charge</summary>
    [SerializeField]
    private float m_minChargePower;

    /// <summary>Time for a full charge</summary>
    [SerializeField]
    private float m_fullChargeTime;

    /// <summary>Power at full charge. This is modified by the power curve and added to the min power.</summary>
    [SerializeField]
    private float m_fullChargePower;

    /// <summary>How charge time maps to charge power</summary>
    [SerializeField]
    private AnimationCurve m_powerCurve;

    /// <summary>Time to cooldown after a shot to begin charging again</summary>
    [SerializeField]
    private float m_cooldownTime;

    // *********** Operating Fields ***************************************************************

    /// <summary>If charging</summary>
    private bool m_bIsCharging = false;

    /// <summary>time spent in current charge</summary>
    private float m_fChargeTime = 0;

    /// <summary>Time left in cooldown</summary>
    private float m_fCooldownTime = 0;

    public override bool BeginFiring() {
        if (m_fCooldownTime <= 0) {
            m_bIsCharging = true;
            m_fChargeTime = 0;
            return true;
        }

        return false;
    }

    public override void EndFiring() {
        if (m_bIsCharging) {
            if (m_fChargeTime >= m_minChargeTime) {
                Shoot();
            }
        }

        m_fChargeTime = 0;
        m_bIsCharging = false;
    }

    public override void Tick(float fDelta) {

        if (m_fCooldownTime > 0) {
            m_fCooldownTime -= fDelta;
            m_weaponUI.SetPct(m_fCooldownTime / m_cooldownTime);
            m_weaponUI.SetChargeMode(GunUI.ChargeMode.Cooldown);
        }

        else if (m_bIsCharging) {
            m_fChargeTime += fDelta;

            if (m_fChargeTime >= m_fullChargeTime) {
                m_fChargeTime = m_fullChargeTime;
                m_weaponUI.SetChargeMode(GunUI.ChargeMode.Charged);
            } else {
                m_weaponUI.SetChargeMode(GunUI.ChargeMode.Charging);
            }

            m_weaponUI.SetPct(m_fChargeTime / m_fullChargeTime);
        } else {
            m_weaponUI.SetPct(0);
        }

        base.Tick(fDelta);
    }

    protected void Shoot() {
        // TODO factor in projectile weight to get velocity
        float fSpeed = m_minChargePower + m_powerCurve.Evaluate(m_fChargeTime / (m_fullChargeTime - m_minChargeTime)) * m_fullChargePower;
        base.Shoot(new Vector3(fSpeed, 0));

        m_fCooldownTime = m_cooldownTime;
    }
}
