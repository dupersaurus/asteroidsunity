﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpaceObject))]
public class BaseEngine : BasePart {
    public ParticleSystem[] m_thrustParticles;
    public float m_thrust;
    
    public float GetThrust() {
        return m_thrust;
    }

    public void StartThrust() {
        for (byte i = 0; i < m_thrustParticles.Length; i++) {
            m_thrustParticles[i].Play();
        }
    }

    public void EndThrust() {
        for (byte i = 0; i < m_thrustParticles.Length; i++) {
            m_thrustParticles[i].Stop();
        }
    }
}
