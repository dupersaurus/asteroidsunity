﻿using UnityEngine;
using System.Collections;

public class Laser : BaseWeapon {

    /// <summary>Maximum single-shot time</summary>
    public float m_maxShotTime;

    /// <summary>Strength of the laser over m_maxShotTime</summary>
    public AnimationCurve m_shotStrength;

    /// <summary>Damage applied per tick of contact</summary>
    public float m_baseDamage;

    private Beam m_beam;

    /// <summary>Time spent on the current shot</summary>
    private float m_fCurrentShotTime;

    private float m_fRecharge = 0;

    public override bool BeginFiring() {
        if (m_fRecharge > 0) {
            return false;
        }

        m_bIsFiring = true;

        if (m_beam == null) {
            m_fCurrentShotTime = 0;
            GameObject beam = ObjectPool.SpawnObject("Beam");

            if (beam != null) {
                m_beam = beam.GetComponent<Beam>();
                UpdateBeam();
            }
        }

        return true;
    }

    public override void EndFiring() {
        if (!m_bIsFiring) {
            return;
        }

        m_bIsFiring = false;
        m_fRecharge = m_maxShotTime;

        if (m_beam != null) {
            m_beam.StopFiring();
            m_beam = null;
        }
    }

    public override void Tick(float fDelta) {
        if (m_fRecharge > 0) {
            m_fRecharge -= fDelta;
            m_weaponUI.SetPct(1 - m_fRecharge / m_maxShotTime);
            m_weaponUI.SetChargeMode(GunUI.ChargeMode.Cooldown);
        } else if (m_bIsFiring) {
            m_fCurrentShotTime += fDelta;
            m_weaponUI.SetPct(1f - m_fCurrentShotTime / m_maxShotTime);
            m_weaponUI.SetChargeMode(GunUI.ChargeMode.Firing);

            if (m_fCurrentShotTime <= m_maxShotTime) {
                UpdateBeam();
            } else {
                EndFiring();
            }
        } else {
            m_weaponUI.SetPct(1);
            m_weaponUI.SetChargeMode(GunUI.ChargeMode.Charged);
        }
    }

    private void UpdateBeam() {
        if (m_beam == null) {
            return;
        }

        Vector3 pos = m_transform.TransformPoint(m_localPosition);
        m_beam.UpdateFiringParams(pos, m_transform.rotation, m_baseDamage * m_shotStrength.Evaluate(m_fCurrentShotTime / m_maxShotTime));
    }
}
