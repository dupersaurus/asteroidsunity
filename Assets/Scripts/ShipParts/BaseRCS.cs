﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpaceObject))]
public class BaseRCS : BasePart {
    public float TranslationForce;
    public float RotationForce;

    public float GetTranslationForce() {
        return TranslationForce;
    }

    public float GetRotationForce() {
        return RotationForce;
    }
}
