﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// template for a MonoBehaviour
/// </summary>
//[RequireComponent(typeof(Hull))]
public class HullSection : MonoBehaviour {

	[System.Serializable]
	protected class Resistance {
		public DamageType Type;

		/// <summary>
		/// Protect against all damage when the factor is less than this.
		/// What factor means depends on the DamageType.
		/// </summary>
		public float MinProtectionFactor;

		/// <summary>
		/// The maximum factor that the resistance mitigates damage for.
		/// </summary>
		public float MaxProtectionFactor;

		/// <summary>
		/// Amount to mitigate incoming damage by when between MinProtectionFactor and MaxProtectionFactor
		/// </summary>
		public float MitigationAmount;

		/// <summary>
		/// Mitigate a given value
		/// </summary>
		/// <param name="fValue"></param>
		/// <returns></returns>
		public float Mitigate(float fValue) {
			if (fValue <= MinProtectionFactor) {
				return 0;
			} else if (fValue > MaxProtectionFactor) {
				return fValue;
			} else {
				return (fValue - MinProtectionFactor) * MitigationAmount;
			}
		}
	}

	/// <summary>
	/// The collider for the section.
	/// TODO Allow multiples?
	/// </summary>
	[SerializeField]
	protected Collider2D m_collider;

	/// <summary>
	/// Resistances to damage provided by the hull.
	/// </summary>
	[SerializeField]
	protected Resistance[] m_resistances;

	/// <summary>
	/// Ship parts assigned to this section
	/// </summary>
	[SerializeField]
	protected BasePart[] m_parts;

    /// <summary>
    /// Health of the armor at full strength
    /// </summary>
    [SerializeField]
    protected float m_armorHP;

    /// <summary>
    /// Health of the structure at full strength
    /// </summary>
    [SerializeField]
    protected float m_structureHP;

    /// <summary>
    /// Connected hull sections
    /// </summary>
    [SerializeField]
    protected HullSection[] m_connections;

    protected float m_fCurrentArmorHP;
    protected float m_fCurrentStructureHP;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake() {
        m_fCurrentArmorHP = m_armorHP;
        m_fCurrentStructureHP = m_structureHP;
    }

    private Resistance GetResistanceByType(DamageType type) {
        for (int i = 0; i < m_resistances.Length; i++) {
            if (m_resistances[i].Type == type) {
                return m_resistances[i];
            }
        }

        return null;
    }

	/// <summary>
	/// Given penetrating projectile, calculate how far it will go
	/// NOTE This assumes that once the projectile passes the armor it slows down at a constant rate
	/// </summary>
	/// <param name="details"></param>
	/// <returns></returns>
	public float CalculatePenetration(PenetratingDamage details) {
		float fMitigated = details.Penetration;

		for (int i = 0; i < m_resistances.Length; i++) {
			if (m_fCurrentArmorHP > 0 && m_resistances[i].Type == DamageType.Penetrating) {
				fMitigated -= m_resistances[i].MinProtectionFactor;
				break;
			}
		}

		if (fMitigated <= 0) {
			return 0;
		}

		// TODO When calculating penetration distance, for no real reason, distance = mitigated penetration factor. Scale as needed.
		return fMitigated * details.Falloff;
	}

    /// <summary>
    /// Process hit to the armor
    /// </summary>
    /// <param name="details"></param>
    /// <param name="enter"></param>
    /// <returns>Amount of damage not absorbed by the armor</returns>
    public float ProcessArmorHit(DamageInstance details, RaycastHit2D enter) {
        float fTotalDamage = CalculateDamageHP(details);
        
        if (m_fCurrentArmorHP >= fTotalDamage) {
            m_fCurrentArmorHP -= fTotalDamage;
            fTotalDamage = 0;
        } else {
            fTotalDamage -= m_fCurrentArmorHP;
            m_fCurrentArmorHP = 0;
        }

        float fColor = m_fCurrentArmorHP / m_armorHP;
        GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, fColor, 1);

        return fTotalDamage;
    }

    /// <summary>
    /// Calculate the combined damage from a damage instance, run through the resistances
    /// </summary>
    /// <param name="details"></param>
    /// <returns></returns>
    protected float CalculateDamageHP(DamageInstance details) {
        Resistance resistance;
        float fTotalDamage = 0;

        //
        if (details.Penetrating != null) {
            resistance = GetResistanceByType(DamageType.Penetrating);

            if (m_fCurrentArmorHP > 0 && resistance != null) {
                fTotalDamage += resistance.Mitigate(details.Penetrating.GetDamage(null));
            }
            else {
                fTotalDamage += details.Penetrating.GetDamage(null);
            }
        }

        //
        if (details.Blast != null) {
            resistance = GetResistanceByType(DamageType.Blast);

            if (m_fCurrentArmorHP > 0 && resistance != null) {
                fTotalDamage += resistance.Mitigate(details.Blast.GetDamage(null));
            }
            else {
                fTotalDamage += details.Blast.GetDamage(null);
            }
        }

        //
        if (details.Thermal != null) {
            resistance = GetResistanceByType(DamageType.Thermal);

            if (m_fCurrentArmorHP > 0 && resistance != null) {
                fTotalDamage += resistance.Mitigate(details.Thermal.GetDamage(null));
            }
            else {
                fTotalDamage += details.Thermal.GetDamage(null);
            }
        }

        return fTotalDamage;
    }

	/// <summary>
	/// Process internal damage from blunt-force or incomplete penetration
	/// </summary>
	/// <param name="details">What's doing the damage</param>
	/// <param name="enter">The main point of impact</param>
	public void ProcessHit(DamageInstance details, RaycastHit2D enter) {
        if (m_structureHP <= 0) {
            return;
        }

        m_fCurrentStructureHP -= CalculateDamageHP(details);

        if (m_fCurrentStructureHP <= 0) {
            OnStructureDestroyed();
        }
        else {
            float fColor = m_fCurrentStructureHP / m_structureHP;
            GetComponentInChildren<SpriteRenderer>().color = new Color(1, fColor, 0, 1);
        }
	}

	/// <summary>
	/// Process internal damage from complete penetration
	/// </summary>
	/// <param name="details">What's doing the damage</param>
	/// <param name="enter">The main point of impact</param>
	/// <param name="exit">The impact's exit point</param>
	public void ProcessHit(DamageInstance details, RaycastHit2D enter, RaycastHit2D exit) {
		//if (exit.fraction == 0) {
			ProcessHit(details, enter);
		/*	return;
		}*/


		//GetComponentInChildren<SpriteRenderer>().color = Color.red;
	}

    /// <summary>
    /// Process internal damage not absorbed by the armor
    /// </summary>
    /// <param name="fAmount"></param>
    public void ProcessOverflowDamage(float fAmount) {
        m_fCurrentStructureHP -= fAmount;

        float fColor = m_fCurrentStructureHP / m_structureHP;
        GetComponentInChildren<SpriteRenderer>().color = new Color(1, fColor, 0, 1);
    }

    protected void OnStructureDestroyed() {
        SendMessageUpwards("HullSectionDestroyed", this);
        collider2D.enabled = false;
        GetComponentInChildren<SpriteRenderer>().color = new Color(0.25f, 0.25f, 0.25f, 1);
    }

    public bool IsDestroyed {
        get { return m_fCurrentStructureHP <= 0; }
    }
}
