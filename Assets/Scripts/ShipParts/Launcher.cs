﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A semi-automatic weapon with an fixed magazine size that must be reloaded.
/// </summary>
public class Launcher : BaseWeapon {

    // *********** Inspector Fields ***************************************************************
    /// <summary>Number of missiles held at once ready to fire.</summary>
    [SerializeField]
    private int m_magazineSize;

    /// <summary>Number of missiles held at once ready to fire</summary>
    public int MagazineSize {
        get { return m_magazineSize; }
    }

    /// <summary>Time to reload the magazine</summary>
    [SerializeField]
    private float m_magazineReloadTime;

    /// <summary>Time to reload the magazine</summary>
    public float MagazineReloadTime {
        get { return m_magazineReloadTime; }
    }

    /// <summary>Minimum time between launches</summary>
    [SerializeField]
    private float m_minLaunchTime = 0;

    /// <summary>Minimum time between launches</summary>
    public float MinimumLaunchTime {
        get { return m_minLaunchTime; }
    }

    /// <summary>Launch port positions. x, y are coordinates, z is angle of launch</summary>
    [SerializeField]
    private Vector3[] m_launchPorts;

    // *********** Operating Fields ***************************************************************
    /// <summary>Only shoot once per input</summary>
    private bool m_bPendingFire = false;

    /// <summary>Time elapsed since the last launch</summary>
    private float m_fLastLaunchTime = 0;

    /// <summary>Number of missiles in the magazine</summary>
    private int m_iMagazineCount = 0;

    /// <summary>Time left in a magazine reload</summary>
    private float m_fMagazineReloadTime = 0;

    /// <summary>Launch port next up for launch</summary>
    private byte m_iPortIndex = 0;

    private bool m_bTargeting = false;
    private SpaceObject m_target;
    private Vector3 m_targetPos;

    protected override void Awake() {
        m_iMagazineCount = MagazineSize;
        base.Awake();
    }

    public override bool BeginFiring() {
        m_bPendingFire = true;
        return true;
    }

    public override void EndFiring() {
        m_bPendingFire = false;
    }

    public override void Tick(float fDelta) {
        m_fLastLaunchTime += fDelta;

        // Reload
        if (m_iMagazineCount <= 0) {
            if (m_fMagazineReloadTime <= 0) {
                m_fMagazineReloadTime = MagazineReloadTime;
            }

            m_fMagazineReloadTime -= fDelta;
            m_weaponUI.SetPct(1f - m_fMagazineReloadTime / MagazineReloadTime);

            if (m_fMagazineReloadTime <= 0) {
                m_weaponUI.SetChargeMode(GunUI.ChargeMode.Charged);
                m_iMagazineCount = MagazineSize;
            } else {
                m_weaponUI.SetChargeMode(GunUI.ChargeMode.Cooldown);
                return;
            }
        }

        float fLaunchPct = (1f - Mathf.Clamp01(m_fLastLaunchTime / MinimumLaunchTime)) / MagazineSize;
        m_weaponUI.SetPct(fLaunchPct + (float)m_iMagazineCount / (float)MagazineSize);

        if (fLaunchPct > 0) {
            m_weaponUI.SetChargeMode(GunUI.ChargeMode.Cooldown);
        } else {
            m_weaponUI.SetChargeMode(GunUI.ChargeMode.Charged);
        }

        if (m_bPendingFire && m_iMagazineCount > 0 && m_fLastLaunchTime >= MinimumLaunchTime) {
            Shoot();
        }

        base.Tick(fDelta);
    }

    protected Projectile Shoot() {
        m_bPendingFire = false;
        m_fLastLaunchTime = 0;
        m_iMagazineCount--;

        // Select port based on best angle to target, starting with next in line.
        // TODO Determine port availability by last launch. Map ports to the magazine. 
        // ex If there are six ports, and six missiles in the magazine, map directly
        // If two ports and six missles, each port has alternating spots in the magazine.
        Vector3 pos = m_launchPorts[m_iPortIndex];
        Quaternion rotation = Quaternion.AngleAxis(pos.z, Vector3.forward) * m_ship.GetRotation();
        Vector3 velocity = Quaternion.AngleAxis(pos.z, Vector3.forward) * Vector3.right;
        pos.z = 0;

        Projectile projectile = base.Shoot(velocity, pos, rotation);

        if (m_bTargeting && projectile != null && projectile is Missile) {
            Missile missile = projectile as Missile;

            if (m_target != null) {
                missile.SetTarget(m_target);
            } else {
                missile.SetTarget(m_targetPos);
            }
        }

        // Next port
        m_iPortIndex++;

        if (m_iPortIndex >= m_launchPorts.Length) {
            m_iPortIndex = 0;
        }

        return null;
    }

    public override void UpdateFiringParams(SpaceObject target, Vector3 position, Vector3 velocity) {
        m_bTargeting = true;
        m_target = target;
        m_targetPos = position;
    }

    public override void UpdateFiringParams(Vector3 position, Vector3 velocity) {
        m_bTargeting = true;
        m_target = null;
        m_targetPos = position;
    }

    void OnDrawGizmos() {
        if (m_transform == null) {
            m_transform = GetComponent<Transform>();
        }

        Vector3 pos;
        float fAngle;


        for (int i = 0; i < m_launchPorts.Length; i++) {
            pos = m_launchPorts[i];
            fAngle = pos.z;
            pos.z = 0;

            pos = m_transform.TransformPoint(pos);

            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(pos, 0.03f);
            Gizmos.DrawRay(pos, (m_transform.rotation * Quaternion.AngleAxis(fAngle, Vector3.forward)) * Vector3.right * 0.3f);
        }
    }
}
