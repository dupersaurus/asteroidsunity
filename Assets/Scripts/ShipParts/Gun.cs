﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A standard automatic weapon that fires as long as the fire command is active
/// </summary>
public class Gun : BaseWeapon {

    // *********** Inspector Fields ***************************************************************
    /// <summary>Rate of fire, seconds between shots</summary>
    [SerializeField]
    private float m_fireRate = 0.1f;

    /// <summary>Velocity of projectile at launch</summary>
    [SerializeField]
    private float m_muzzleVelocity = 8;

    /// <summary>In radians</summary>
    [SerializeField]
    private float m_spreadAngle = 0;

    // *********** Operating Fields ***************************************************************
    private float m_fTimeSinceLastFire = 0;

    protected override void Awake() {
        base.Awake();
        m_bIsFiring = false;
    }

    public override bool BeginFiring() {
        m_bIsFiring = true;
        return true;
    }

    public override void EndFiring() {
        m_bIsFiring = false;
    }

    public override void Tick(float fDelta) {
        base.Tick(fDelta);

        m_fTimeSinceLastFire += fDelta;

        if (m_bIsFiring && m_fTimeSinceLastFire >= m_fireRate) {
            Shoot();
        }

        m_weaponUI.SetPct(m_fTimeSinceLastFire / m_fireRate);
    }

    /// <summary>
    /// Calculate lead amount and pass up a position to target
    /// </summary>
    /// <param name="target"></param>
    /// <param name="position"></param>
    /// <param name="velocity"></param>
    public override void UpdateFiringParams(SpaceObject target, Vector3 position, Vector3 velocity) {
        base.UpdateFiringParams(position);
    }

    public override void UpdateFiringParams(Vector3 position, Vector3 velocity) {
        base.UpdateFiringParams(position);
    }

    protected Projectile Shoot() {
        if (!CanFireArc()) {
            return null;
        }

        Quaternion rotation = Quaternion.AngleAxis(Random.value * m_spreadAngle - m_spreadAngle * 0.5f, Vector3.forward);

        m_fTimeSinceLastFire = 0;
        return base.Shoot(rotation * m_zeroFacing * m_muzzleVelocity);
    }
}
