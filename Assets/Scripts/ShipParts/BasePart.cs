﻿using UnityEngine;
using System.Collections;

public class BasePart : MonoBehaviour {
    protected Transform m_transform;
    protected SpaceObject m_ship;

    protected virtual void Awake() {
        m_transform = transform;
        m_ship = GetComponent<SpaceObject>();
    }

    public virtual void Tick(float fDelta) {

    }
}
