﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireControl : BasePart {

    protected TargetCaret m_caret = null;
    protected SpaceObject m_target = null;

    protected BaseWeapon[] m_weapons;

    protected override void Awake() {
        base.Awake();
        m_weapons = GetComponents<BaseWeapon>();

        Object prefab = Resources.Load("Target Cursor");
        m_caret = (Instantiate(prefab) as GameObject).GetComponent<TargetCaret>();
    }

    public override void Tick(float fDelta) {
        base.Tick(fDelta);

        Vector3 pos = Vector3.zero;
        Vector3 vel = Vector3.zero;

        if (m_target != null) {
            pos = m_target.GetPosition();
            vel = m_target.GetVelocity();
        } else {
            pos = m_caret.TargetPos;
        }

        // TODO add fuzziness based on controller skill

        for (int i = 0; i < m_weapons.Length; i++) {
            if (m_target != null) {
                m_weapons[i].UpdateFiringParams(m_target, pos, vel);
            } else {
                m_weapons[i].UpdateFiringParams(pos, Vector3.zero);
            }
        }
    }

    public SpaceObject LockedTarget {
        get { return m_target; }
    }
}
