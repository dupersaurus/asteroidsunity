﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Description of a SpaceObject's structural integrety. At the very least, describes hit points.
/// </summary>
public class Hull : BasePart {

	/// <summary>
	/// All sections that make up the ship
	/// </summary>
	[SerializeField]
	protected HullSection[] m_sections;

    /// <summary>
    /// List of sections that make up the keel of the ship. If a section of the keel is destroyed,
    /// the ship is dead.
    /// If a part of the keel is made of multiple sections, all sections have to be destroyed.
    /// </summary>
    [SerializeField]
    protected List<KeelSection> m_keelSections;

    [System.Serializable]
    protected struct KeelSection {
        public List<HullSection> Sections;
    }

	/// <summary>
	/// Called by projectiles on collision with the space object
	/// </summary>
	/// <param name="damage">Information about the damage being applied</param>
	/// <param name="hit">Parameters of the collision</param>
	/// <returns>The fraction of damage absorbed by the SpaceObject, roughly equal to damageTaken / projectileDamage</returns>
	public float ProcessDamage(Vector3 velocity, DamageInstance damage, RaycastHit2D hit) {

		// Find sections impacted by the hit.
		// Forward trace is from impact point in to find enter positions.
		// Backward trace is from end of ray out to find exit positions, if applicable
		RaycastHit2D[] forward;
		RaycastHit2D[] backward;
		HullSection initial = hit.collider.GetComponent<HullSection>();
        float fDistance = 0;

		// If penetrating, calculate distance and trace along it
		if (damage.Penetrating != null) {
			fDistance = initial.CalculatePenetration(damage.Penetrating);

            // If there is a fuse, decrease penetration distance to that
            if (damage.Blast != null && damage.Blast.FuseLength < fDistance) {
                fDistance = damage.Blast.FuseLength;
            }

            Debug.Log("Penetration factor => " + damage.Penetrating.Penetration + ", distance => " + fDistance);

			if (fDistance > 0) {
				forward = Physics2D.RaycastAll(hit.point, velocity, fDistance);
				backward = Physics2D.RaycastAll(hit.point + (Vector2)velocity.normalized * fDistance, velocity * -1, fDistance);
			} else {
				forward = new RaycastHit2D[0];
				backward = new RaycastHit2D[0];
			}

            // Damage to armor on contact
            float fRemaining = initial.ProcessArmorHit(damage, hit);

            // Apply to interior
            if (fRemaining > 0) {
                initial.ProcessOverflowDamage(fRemaining);
            }

            // TODO Account for how all targets may not take all damage? Case: penetrating projectile that explodes at some point.
            // apply internal damage from penetrating projectiles
            for (int i = 0; i < forward.Length; i++) {
                forward[i].collider.GetComponent<HullSection>().ProcessHit(damage, forward[i], backward[backward.Length - i - 1]);
            }

            if (backward.Length > 0 && backward[0].fraction > 0) {
                SpawnExitParticle(backward[0], velocity);
            }
		}  
        
        // Otherwise, surface damage
        else {
			forward = new RaycastHit2D[1];
			forward[0] = hit;

			backward = new RaycastHit2D[0];

            // Damage to armor on contact
            float fRemaining = initial.ProcessArmorHit(damage, hit);

            // Apply to interior
            if (fRemaining > 0) {
                initial.ProcessOverflowDamage(fRemaining);
            }
		}

		

        if (fDistance == 0) {
            return 2f;
        }
        else {
            return 1f;
        }
    }

    protected void SpawnExitParticle(RaycastHit2D exit, Vector3 velocity) {
        GameObject particle = ObjectPool.SpawnObject("Exit Hit");

        if (particle != null && m_transform != null) {
            Transform particleTrans = particle.transform;
            particleTrans.parent = m_transform.parent;
            particleTrans.position = exit.point;
            particleTrans.rotation = Quaternion.LookRotation(velocity);
        }
    }

    /// <summary>
    /// Called by a hull section when it's structure has been destroyed
    /// </summary>
    /// <param name="target"></param>
    protected void HullSectionDestroyed(HullSection target) {
        bool bSectionsDestroyed = false;
        bool bTargetSection = false;
        int iSplitIndex = 0;
        
        for (int i = 0; i < m_keelSections.Count; i++) {
            bSectionsDestroyed = false;

            for (int j = 0; j < m_keelSections[i].Sections.Count; j++) {
                bSectionsDestroyed |= m_keelSections[i].Sections[j].IsDestroyed;

                if (m_keelSections[i].Sections[j] == target) {
                    bTargetSection = true;
                    iSplitIndex = j;
                }
            }

            if (bTargetSection) {

                // TODO ship destroyed
                if (bSectionsDestroyed) {

                }

                break;
            }
        }
    }
}
