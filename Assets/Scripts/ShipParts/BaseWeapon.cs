﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpaceObject))]
public class BaseWeapon : BasePart {
    protected Transform m_parentTransform;

    public string hintName;

    /// <summary>Position of the weapon in the local space of the ship</summary>
    [SerializeField]
    protected Vector3 m_localPosition;

    /// <summary>Pool id of the projectile to use</summary>
    [SerializeField]
    protected string m_projectilePath = "Bullet";

    /// <summary>Name of prefab to use for the weapon's UI</summary>
    [SerializeField]
    protected string m_uiPrefab = "Gun UI";

    /// <summary>Direction the weapon faces at zero rotation, in ship space.</summary>
    [SerializeField]
    protected Vector2 m_zeroFacing = new Vector2(1, 0);

    protected Quaternion m_zeroRotOffset;

    [SerializeField]
    protected bool m_useGimbal = false;

    /// <summary>Cone around zero facing the weapon can target, in degrees</summary>
    [SerializeField]
    protected float m_gimbalRange = 0;

    /// <summary>Whether to allow firing when target is outside of the weapon's arc</summary>
    [SerializeField]
    protected bool m_allowOutArcFire = false;

    /// <summary>Whether target is currently in firing arc</summary>
    protected bool m_bIsInFiringArc = false;

    /// <summary>Current angle of the gimbal</summary>
    protected Quaternion m_gimbalRot = Quaternion.identity;

    protected bool m_bIsFiring;

    protected GunUI m_weaponUI;

    protected override void Awake() {
        base.Awake();
        m_parentTransform = m_ship.transform;
        m_zeroRotOffset = Quaternion.FromToRotation(Vector3.right, m_zeroFacing);

        /*float fAngle = Vector3.Angle(Vector3.right, m_zeroFacing);

        if (Vector3.Cross(Vector3.right, m_zeroFacing).z >= 0) {
            m_zeroRotOffset = Quaternion.AngleAxis(fAngle, Vector3.forward);
        }
        else {
            m_zeroRotOffset = Quaternion.AngleAxis(-fAngle, Vector3.forward);
        }*/
    }

    protected virtual void Start() {
        
    }

    public virtual void AddWeaponUI() {
        m_weaponUI = UI.Instance.AddWeaponUI(m_uiPrefab);
    }

    /// <summary>
    /// Begin firing the weapon
    /// <returns>True if firing allowed, false if not</returns>
    /// </summary>
    public virtual bool BeginFiring() {
        return true;
    }

    /// <summary>
    /// Stop firing the weapon
    /// </summary>
    public virtual void EndFiring() {

    }

    /// <summary>
    /// Keep firing arc calculated
    /// </summary>
    /// <param name="fDelta"></param>
    public override void Tick(float fDelta) {
        base.Tick(fDelta);

        
    }

    protected virtual Projectile Shoot(Vector3 muzzleVelocity) {
        return Shoot(muzzleVelocity, m_localPosition, GetWeaponRotation());
    }

    /// <summary>
    /// Launch a projectile
    /// </summary>
    /// <param name="muzzleVelocity">Velocity out of the weapon. Factor in weapon rotation, but not ship rotation.</param>
    protected virtual Projectile Shoot(Vector3 muzzleVelocity, Vector3 launchPos, Quaternion rotation) {
        //Debug.Log(hintName + " >> " + m_bIsInFiringArc + ", " + m_allowOutArcFire);

        if (!CanFireArc()) {
            return null;
        }

        Vector3 pos = m_transform.TransformPoint(launchPos);

        // Fire
        Vector3 velocity = m_transform.rotation * m_gimbalRot * muzzleVelocity + m_ship.GetVelocity();

        GameObject obj = ObjectPool.SpawnObject(m_projectilePath);

        if (obj != null) {
            Projectile projectile = obj.GetComponent<Projectile>();
            Game.Instance.AddTicker(projectile);
            projectile.SetLaunchParameters(pos, velocity, rotation);
            projectile.Launcher = m_parentTransform;

            return projectile;
        }

        return null;
    }

    /// <summary>
    /// Aim the weapon to face a given position. 
    /// </summary>
    /// <param name="position"></param>
    public virtual void UpdateFiringParams(Vector3 position) {
        if (!m_useGimbal) {
            m_gimbalRot = Quaternion.identity;
            return;
        }

        Vector3 pos = m_transform.TransformPoint(m_localPosition);
        Vector3 target = position - pos;   // Pos relative to gun
        Vector3 weaponZero = m_ship.GetRotation() * m_zeroFacing;

        pos.z = 0;
        target.z = 0;

        float fAngle = Vector3.Angle(weaponZero, target);

        if (fAngle > m_gimbalRange) {
            fAngle = m_gimbalRange;
            m_bIsInFiringArc = false;
        }
        else {
            m_bIsInFiringArc = true;
        }

        float fCross = Vector3.Cross(weaponZero, target).z;

        //Debug.Log("Angle: " + fAngle + ", Cross: " + fCross);

        fAngle = Mathf.Clamp(fAngle, 0, m_gimbalRange);

        if (fCross < 0) {
            fAngle *= -1;
        }

        m_gimbalRot = Quaternion.AngleAxis(fAngle, Vector3.forward);
    }

    /// <summary>
    /// Aim the weapon at a position moving a certain velocity
    /// </summary>
    /// <param name="position"></param>
    /// <param name="velocity"></param>
    public virtual void UpdateFiringParams(Vector3 position, Vector3 velocity) {

    }

    /// <summary>
    /// Aim the weapon at an object, with a position and velocity
    /// </summary>
    /// <param name="target"></param>
    /// <param name="position"></param>
    /// <param name="velocity"></param>
    public virtual void UpdateFiringParams(SpaceObject target, Vector3 position, Vector3 velocity) {

    }

    protected virtual Quaternion GetWeaponRotation() {
        return m_ship.GetRotation() * m_zeroRotOffset * m_gimbalRot;
    }

    /// <summary>
    /// Whether the weapon can fire based on its weapon arc
    /// </summary>
    /// <returns></returns>
    protected bool CanFireArc() {
        return !m_useGimbal || m_bIsInFiringArc || m_allowOutArcFire;
    }

    public void OnDrawGizmos() {
        if (m_transform == null) {
            m_transform = GetComponent<Transform>();
        }

        Vector3 pos = m_transform.TransformPoint(m_localPosition);

        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(pos, 0.05f);

        if (m_gimbalRange == 0) {
            Gizmos.DrawRay(pos, m_transform.rotation * m_zeroFacing * 0.3f);
        } else {
            Quaternion gimbalRot = m_gimbalRot;
            Gizmos.DrawRay(pos, (m_transform.rotation * gimbalRot) * m_zeroFacing * 0.3f);
            Gizmos.DrawRay(pos, (m_transform.rotation * Quaternion.AngleAxis(m_gimbalRange, Vector3.forward)) * m_zeroFacing * 0.3f);
            Gizmos.DrawRay(pos, (m_transform.rotation * Quaternion.AngleAxis(-m_gimbalRange, Vector3.forward)) * m_zeroFacing * 0.3f);
        }
    }
}
