﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

    public SpaceObject m_target;
    public Rect m_deadzone;

    private Camera m_camera;
    private Transform m_transform;

	// Use this for initialization
	void Start () {
        m_transform = transform;
        m_camera = camera;
	}
	
	// Update is called once per frame
	void Update () {
        if (m_target != null) {
            Vector3 pos = m_target.GetPosition();
            Vector3 viewport = m_camera.WorldToViewportPoint(pos);

            if (!m_deadzone.Contains(viewport)) {
                m_transform.position += m_target.GetVelocity() * Time.deltaTime;

                // Zoom camera out
                /*float fBiggest = Mathf.Max(m_deadzone.xMin - viewport.x, viewport.x - m_deadzone.xMax, m_deadzone.yMin - viewport.y, viewport.y - m_deadzone.yMax);
                m_camera.orthographicSize = Mathf.Clamp(5 + 3 * fBiggest, 5, 8);
                Debug.Log("Biggest >> " + fBiggest);*/

                /*m_camera.orthographicSize = Mathf.Clamp(5 + m_target.GetVelocity().magnitude, 5, 8);
                Debug.Log("Speed >> " + m_target.GetVelocity().magnitude);*/

            } else {
                m_camera.orthographicSize = 5;
            }
        }
	}
}
